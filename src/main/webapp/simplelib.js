var LINK_REMOVE =       "rest/catalog/remove";
var LINK_GET =          "rest/catalog/allBooks";
var CHANGE_CATALOG =    "rest/catalog/changeCatalog";
var EDIT_BOOK =         "rest/catalog/editBook";
var LINK_GET_BY_NAME =  "rest/catalog/getByName";

$(document).ready(function() {
    $('#addBook').submit(function(e) {
        $('.bookMessage').remove();
        if ($(this).find( "input[name='name']" ).val() == "") {
            $('#addBook')
                .append(
                '<p class="bookMessage">Введите имя!</p>');
            return false;
        }
        var catalog = $(this).find( "select[name='selectCatalog']" ).val();
        var name = $(this).find( "input[name='name']" ).val();
        var newAuthor = $(this).find( "input[name='author']" ).val();
        var date = $(this).find( "input[name='date']" ).val();
        var formURL = $(this).attr("action");
        var request = {
            catalog : catalog,
            newName : name,
            newAuthor : newAuthor,
            date : date
        };
        $.ajax({
            url : formURL,
            type : "POST",
            contentType: "application/json; charset=utf-8",
            data : JSON.stringify(request),
            success : function(data, textStatus, jqXHR) {
                $('#addBook').append(
                    '<p class="bookMessage">Сохранено</p>');
                render();
            },
            error : function(jqXHR, textStatus, errorThrown) {
                $('#addBook').append(
                    '<p class="bookMessage">Ошибка!</p>');
            }
        });
        e.preventDefault(); // STOP default action
    });

    $('#getAllBooks').submit(function(e){
        render();
        e.preventDefault(); // STOP default action
    });

    $('#getByName').submit(function(e){
        renderByName();
        e.preventDefault(); // STOP default action
    });

    $(".datepicker").datepicker({ monthNames:
        ["Январь","Февраль","Март","Апрель","Май","Июнь","Июль","Август",
            "Сентябрь","Октябрь","Ноябрь","Декабрь"],
        dayNamesMin: ["Пн", "Вт", "Ср", "Чт", "Пт", "Сб", "Вс"]
    });
    //setInterval('render()',10000);
});

function editBook(index) {
    $('#books').remove();
    $('.bookMessage').remove();
    $('#addBook').hide(1000);
    $('#editBook').show(1000);
    $('#editBook').submit(function(e) {
        if ($(this).find("input[name='name']").val() == "") {
            $('#editBook')
                .append('<p class="bookMessage">Введите имя!</p>');
            return false;
        };
        var newName = $(this).find("input[name='name']").val();
        var newAuthor = $(this).find("input[name='author']").val();
        var date = $(this).find("input[name='date']").val();
        var formURL = EDIT_BOOK;
        var request = {
            newName: newName,
            newAuthor: newAuthor,
            date: formatDate(date),
            index: index
        };
        alert(JSON.stringify(request));
        $.ajax({
            url: formURL,
            type: "POST",
            contentType: "application/json; charset=utf-8",
            data: JSON.stringify(request),
            success: function (data, textStatus, jqXHR) {
                $('#editBook').append(
                    '<p class="bookMessage">Сохранено</p>');
                $('#addBook').show(1000);
                $('#editBook').hide(1000);
                location.reload();
            },
            error: function (jqXHR, textStatus, errorThrown) {
                $('#editBook').append(
                    '<p class="bookMessage">Ошибка!</p>');
            }
        });
        e.preventDefault(); // STOP default action
    });
}

function renderByName() {
    var name = $('#getByName').find( "input[name='name']" ).val();
    $('#books').remove();
    var request = {
        name: name
    };
    alert(JSON.stringify(request));
    $.ajax({
        url : LINK_GET_BY_NAME,
        data :JSON.stringify(request),
        contentType: "application/json; charset=utf-8",
        type : "POST",
        dataType : "json",
        success : renderMap,
        error : function(jqXHR, textStatus, errorThrown) {
        }
    });
};

function render() {
    var catalog = $('#getAllBooks').find( "input[name='catalog']:checked" ).val();
    $('#books').remove();
    var request = {
        catalog : catalog
    };
    $.ajax({
        url : LINK_GET,
        data :JSON.stringify(request),
        contentType: "application/json; charset=utf-8",
        type : "POST",
        dataType : "json",
        success : renderMap,
        error : function(jqXHR, textStatus, errorThrown) {
        }
    });
};

function renderMap(data) {
    $('#bookTable').append("<table id='books'></table>");
    $('#books').append(
        '<tr><td>  Ключ(Index) </td><td> Название </td><td> Автор </td><td> Текущий каталог </td><td> Изменить каталог </td><td>' +
        ' Дата </td><td> Редактировать </td><td> Удалить </td></tr>'
    );
    for (var key in data) {
        if (data.hasOwnProperty(key)) {
            var book = data[key];
            $('#books').append(
                '<tr><td>' +
                key + '</td><td>' +
                book.name + ' </td><td>' +
                book.author + ' </td><td>' +
                book.catalogType + ' </td><td><button onclick= "changeCatalog(' + key + ')">change catalog</button></td><td>' +
                formatDate(book.date) + ' </td><td><button onclick= "editBook(' + key + ')">edit</button></td>' +
                '<td><button onclick= "removeBook(' + key + ')">delete</button></td></tr>');
        }
    }
};

function changeCatalog(index, catalog) {
    $('#books').remove();
    var formURL = CHANGE_CATALOG;
    var catalog = $('#getAllBooks').find( "input[name='catalog']:checked" ).val();
    var getListURL = $('#getAllBooks').attr("action");
    alert(getListURL);
    var request = {
        index : index,
        catalog : catalog
    };
    $.ajax({
        url : formURL,
        data :JSON.stringify(request),
        contentType: "application/json; charset=utf-8",
        type : "POST",
        dataType : "json",
        success : render,
        error : function(jqXHR, textStatus, errorThrown) {
        }
    });
}

function removeBook(index, catalog) {
    $('#books').remove();
    var formURL = LINK_REMOVE;
    var catalog = $('#getAllBooks').find( "input[name='catalog']:checked" ).val();
    var getListURL = $('#getAllBooks').attr("action");
    var request = {
        index : index,
        catalog : catalog
    };
    $.ajax({
        url : formURL,
        data :JSON.stringify(request),
        contentType: "application/json; charset=utf-8",
        type : "POST",
        dataType : "json",
        success : render,
        error : function(jqXHR, textStatus, errorThrown) {
        }
    });
}

function formatDate(date) {
    var d = new Date(date);

    var dd = d.getDate();
    var mm = d.getMonth() + 1;
    var yyyy = d.getFullYear();

    if (dd < 10) dd = '0' + dd;
    if (mm < 10) mm = '0' + mm;
    if (yyyy < 10) yyyy = '0' + yyyy;

    return dd + '/' + mm + '/' + yyyy;
}