package com.alexander.library.logic;

import com.alexander.library.entity.Book;
import com.alexander.library.entity.Catalog;
import com.alexander.library.enums.CatalogType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import javax.annotation.PostConstruct;
import javax.ejb.Singleton;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * Created by alexander on 16.10.16.
 */
@Singleton
public class BookService {

    private final Logger logger = LoggerFactory.getLogger(BookService.class);

    private static final String DATE_FORMAT = "dd/mm/yyyy";

    private Catalog unionCatalog;

    private AtomicInteger key;

    @PostConstruct
    private void init() {
        unionCatalog = new Catalog(new HashMap<Integer, Book>());
        key = new AtomicInteger(0);
        initBooks();
        logger.info("catalogs initialized");
    }

    private void initBooks() {
        HashMap<Integer, Book> books = new HashMap<Integer, Book>();
        books.put(key.getAndAdd(1), new Book("Евгений Онегин", "А С Пушкин", new Date(), CatalogType.publicCatalog));
        books.put(key.getAndAdd(1), new Book("Преступление и наказание", "Ф М Достоевский", new Date(), CatalogType.publicCatalog));
        books.put(key.getAndAdd(1), new Book("Война и Мир", "Л Н Толстой", new Date(), CatalogType.privateCatalog));
        books.put(key.getAndAdd(1), new Book("Три мушкетёра", "Дюма", new Date(), CatalogType.privateCatalog));
        unionCatalog.getBooks().putAll(books);
    }

    public void addNewBook(String catalog, String name, String author, String date){
        CatalogType catalogType = CatalogType.getCatalogType(catalog);
        DateFormat format = new SimpleDateFormat(DATE_FORMAT);
        Date dateIssue = null;
        try {
            dateIssue = format.parse(date);
        } catch (ParseException e) {
            logger.error("Wrong date format");
        }

        System.out.println(dateIssue);
        unionCatalog.getBooks().put(key.getAndAdd(1), new Book(name, author, dateIssue, catalogType));
        System.out.println("books = " + unionCatalog.getBooks());
    }

    public void changeCatalog(Integer index){
        CatalogType catalogType = unionCatalog.getBooks().get(index).getCatalogType();
        if(catalogType == CatalogType.publicCatalog)
            unionCatalog.getBooks().get(index).setCatalogType(CatalogType.privateCatalog);
        if(catalogType == CatalogType.privateCatalog)
            unionCatalog.getBooks().get(index).setCatalogType(CatalogType.publicCatalog);
    }

    public void editBook(Integer index, String newName, String newAuthor, Date newDate) throws Exception {
        Book book = unionCatalog.getBooks().get(index);
        editBook(book, newName, newAuthor, newDate);
    }

    public void removeBook(Integer index){
        unionCatalog.getBooks().remove((int) index);
    }

    public Map<Integer, Book> getBooks(String catalog) {
        CatalogType catalogType = CatalogType.getCatalogType(catalog);
        switch (catalogType){
            case unionCatalog:
                return unionCatalog.getBooks();
            case privateCatalog:
            case publicCatalog:
                Map<Integer, Book> listBooks = new HashMap<Integer, Book>();
                for (Integer k: unionCatalog.getBooks().keySet()) {
                    Book book = unionCatalog.getBooks().get(k);
                    if (book.getCatalogType() == catalogType)
                        listBooks.put(k, book);
                }
                return listBooks;
        }
        return null;
    }

    public void editBook(Book book, String newName, String newAuthor, Date newDate) throws Exception {
        if(book == null)
            throw new Exception("Book is absent");
        if (newName != null && !newName.isEmpty())
            book.setName(newName);
        if (newAuthor != null && !newAuthor.isEmpty())
            book.setAuthor(newAuthor);
        if (newDate != null)
            book.setDate(newDate);
    }

    public Map<Integer, Book> getByName(String name) {
        Map<Integer, Book> listBooks = new HashMap<Integer, Book>();
        for (Integer k: unionCatalog.getBooks().keySet()) {
            Book book = unionCatalog.getBooks().get(k);
            if(book.getName().contains(name)){
                listBooks.put(k, book);
            }
        }
        return listBooks;
    }
}