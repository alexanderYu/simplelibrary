package com.alexander.library.enums;

/**
 * Created by alexander on 16.10.16.
 */
public enum CatalogType {

    unionCatalog("allBooks"),
    publicCatalog("public"),
    privateCatalog("private");
    private String code;

    CatalogType(String code) {
        this.code = code;
    }

    public String getCode() {
        return this.code;
    }

    public static CatalogType getCatalogType(String code) {
        if (code != null) {
            for (CatalogType b : CatalogType.values()) {
                if (code.equalsIgnoreCase(b.code)) {
                    return b;
                }
            }
        }
        return null;
    }
}
