package com.alexander.library.controllers;

import com.alexander.library.dto.RequestDto;
import com.alexander.library.entity.Book;
import com.alexander.library.logic.BookService;
import org.apache.commons.lang.StringUtils;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ws.rs.*;
import javax.ws.rs.core.Response;
import java.util.Map;

import static javax.ws.rs.core.MediaType.APPLICATION_JSON;

/**
 * Created by alexander on 09.10.16.
 */
@Path(value = "/catalog")
@Stateless
public class CatalogController {

    @EJB
    private BookService bookService;

    @POST
    @Path("/addBook")
    @Consumes(APPLICATION_JSON)
    @Produces( APPLICATION_JSON )
    public Response addBook(RequestDto requestDto) {
        bookService.addNewBook(requestDto.catalog, requestDto.newName, requestDto.newAuthor, requestDto.date);
        return Response.noContent().build();
    }

    @POST
    @Path("/allBooks")
    @Consumes(APPLICATION_JSON)
    @Produces( APPLICATION_JSON )
    public Map<Integer, Book> getBooksPost(RequestDto requestDto) {
        return bookService.getBooks(requestDto.catalog);
    }

    @Path("/remove")
    @POST
    @Consumes(APPLICATION_JSON)
    @Produces(APPLICATION_JSON)
    public Response remove(RequestDto requestDto) {
        bookService.removeBook(requestDto.index);
        return Response.noContent().build();
    }

    @Path("/changeCatalog")
    @POST
    @Consumes(APPLICATION_JSON)
    @Produces(APPLICATION_JSON)
    public Response changeCatalog(RequestDto requestDto) {
        bookService.changeCatalog(requestDto.index);
        return Response.noContent().build();
    }

    @Path("/editBook")
    @POST
    @Consumes(APPLICATION_JSON)
    @Produces(APPLICATION_JSON)
    public Response editBook(RequestDto requestDto) throws Exception {
        System.out.println("requestDto = " + requestDto);
        bookService.editBook(requestDto.index, requestDto.newName, requestDto.newAuthor, requestDto.newDate);
        return Response.noContent().build();
    }


    @Path("/getByName")
    @POST
    @Consumes(APPLICATION_JSON)
    @Produces({ APPLICATION_JSON })
    public Map<Integer, Book> get(RequestDto requestDto) throws Exception {
        return bookService.getByName(requestDto.name);
    }
}
