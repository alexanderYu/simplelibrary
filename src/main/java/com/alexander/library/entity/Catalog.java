package com.alexander.library.entity;

import com.alexander.library.enums.CatalogType;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Created by alexander on 16.10.16.
 */
public class Catalog {

    private HashMap<Integer, Book> books;

    public Catalog(HashMap<Integer, Book> books) {
        this.books = books;
    }

    public HashMap<Integer, Book> getBooks() {
        return books;
    }

    public void setBooks(HashMap<Integer, Book> books) {
        this.books = books;
    }

    @Override
    public String toString() {
        return "Catalog{" +
                "books=" + books +
                '}';
    }
}
