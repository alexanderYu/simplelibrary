package com.alexander.library.entity;

import com.alexander.library.enums.CatalogType;

import java.util.Date;

/**
 * Created by alexander on 09.10.16.
 */
public class Book {
    private String name;
    private String author;
    private Date date;
    private CatalogType catalogType;

    public Book(String name, String author, Date date, CatalogType catalogType) {
        this.name = name;
        this.author = author;
        this.date = date;
        this.catalogType = catalogType;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public CatalogType getCatalogType() {
        return catalogType;
    }

    public void setCatalogType(CatalogType catalogType) {
        this.catalogType = catalogType;
    }

    @Override
    public String toString() {
        return "Book{" +
                "name='" + name + '\'' +
                ", author='" + author + '\'' +
                ", date=" + date +
                ", catalogType=" + catalogType +
                '}';
    }
}
