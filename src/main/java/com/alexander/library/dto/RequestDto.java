package com.alexander.library.dto;

import java.util.Date;

/**
 * Created by alexander on 16.10.16.
 */
public class RequestDto {
    public Integer index;
    public String catalog;
    public String newName;
    public String newAuthor;
    public String name;
    public Date newDate;
    public String date;

    @Override
    public String toString() {
        return "RequestDto{" +
                "index=" + index +
                ", catalog='" + catalog + '\'' +
                ", newName='" + newName + '\'' +
                ", newAuthor='" + newAuthor + '\'' +
                ", name='" + name + '\'' +
                ", newDate=" + newDate +
                ", date='" + date + '\'' +
                '}';
    }
}
